package com.kaledboug.firstlibrary.fragment

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.text.bold
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.kaledboug.firstlibrary.R
import com.kaledboug.firstlibrary.databinding.FragmentGameBinding
import com.kaledboug.firstlibrary.model.ImageForAdapter
import com.kaledboug.firstlibrary.model.Params
import com.kaledboug.firstlibrary.model.Request
import com.kaledboug.firstlibrary.model.ResponseChallenge
import com.kaledboug.firstlibrary.utils.ChallengeListAdapter
import com.kaledboug.firstlibrary.utils.DataState
import com.kaledboug.firstlibrary.viewmodel.ViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

@AndroidEntryPoint
class GameFragment : Fragment() {

    private var reset: Boolean = false
    private lateinit var binding: FragmentGameBinding
    private val viewModel: ViewModel by viewModels()

    private var numberOne: String = ""
    private var numberTwo: String = ""

    private var idChallenge: String = ""
    private var challengeMode: String = ""

    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    private var imagesSelected: MutableList<String> = mutableListOf()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentGameBinding.inflate(inflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.editTextTranscription.visibility = View.INVISIBLE
        binding.textTranscription.visibility = View.INVISIBLE


        if (viewModel.response.value == null || viewModel.response.value is DataState.Error) {
            viewModel.getChallenge("demo_game", requireContext().applicationContext)
        }

        setUpCheckChallengeObserver()
        setUpGetChallengeObserver()
        setUpStepObserver()

        binding.buttonValidate.setOnClickListener {
            getAnswersFromInputOrSelection()
            binding.editTextTranscription.text.clear()
        }

        binding.info?.setOnClickListener{
            findNavController().navigate(R.id.action_gameFragment_to_informationFragment)
        }

        binding.refresh?.setOnClickListener {
            imagesSelected.clear()
            viewModel.getChallenge("demo_game", requireContext().applicationContext)
            binding.editTextTranscription?.text?.clear()
        }

    }



    private fun setUpStepObserver() {
        viewModel.step.observe(viewLifecycleOwner, { step ->

            if (step < 5) {
                binding.textStep.text = "Challenge N°${step + 1}/5"

            } else {
                binding.textStep.text = "Challenge N°${step}/5"
            }
            /* if (step == 0) {
                 score = 0
             }*/
            if (step == 5) {

                val score = viewModel.score.value
                val action = score?.let {
                    GameFragmentDirections.actionGameFragmentToScoreFragment(
                        it
                    )
                }

                if (action != null) {

                    findNavController().navigate(action)
                }

                viewModel.updateStep(true)
                viewModel.updateScore(true)
            }
        })
    }

    private fun getAnswersFromInputOrSelection() {
        if (challengeMode == "transcriptions_words") {
            if (binding.editTextTranscription.text.toString() != "") {
                val answer = binding.editTextTranscription.text
                val answerArray = answer.split(" ")
                if (answerArray.size == 2) {
                    val answerOne = answerArray[0]
                    val answerTwo = answerArray[1]

                    val transcriptions = mapOf(
                        numberOne to answerOne,
                        numberTwo to answerTwo
                    )
                    val selection: List<String> = emptyList()
                    val params: Params = Params(
                        selection,
                        transcriptions
                    )
                    val request: Request = Request(params)
                    checkChallenge(request)
                } else if (answerArray.size == 1) {
                    val answerOne = answerArray[0]

                    val transcriptions = mapOf(
                        numberOne to answerOne,
                        numberTwo to ""
                    )
                    val selection: List<String> = emptyList()
                    val params: Params = Params(
                        selection,
                        transcriptions
                    )
                    val request: Request = Request(params)
                    checkChallenge(request)
                }
            } else {
                Toast.makeText(requireContext(), "Please enter the answer", Toast.LENGTH_SHORT)
                    .show()
            }

        } else {
            var selection = emptyList<String>()
            if (imagesSelected.size == 0) {
                selection = listOf(
                    "",
                    "",
                    "",
                )
            }
            if (imagesSelected.size == 1) {
                selection = listOf(
                    imagesSelected[0],
                    "",
                    "",
                )
            }
            if (imagesSelected.size == 2) {
                selection = listOf(
                    imagesSelected[0],
                    imagesSelected[1],
                    "",
                )
            }
            if (imagesSelected.size == 3) {
                selection = listOf(
                    imagesSelected[0],
                    imagesSelected[1],
                    imagesSelected[2],
                )
            }

            val transcriptions = emptyMap<String, String>()
            val params = Params(
                selection,
                transcriptions
            )
            val request = Request(params)

            checkChallenge(request)
        }

    }

    private fun checkChallenge(request: Request) {
        viewModel
            .checkChallenge(
                id_client = "demo_game",
                id = idChallenge,
                request,
                requireContext().applicationContext
            )
        viewModel.getChallenge("demo_game", requireContext().applicationContext)
    }

    private fun setUpCheckChallengeObserver() {
        viewModel.validate.observe(viewLifecycleOwner, Observer {
            when (it) {
                is DataState.Loading -> {
                }
                is DataState.Error -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                        .show()
                    viewModel.reset()
                }
                is DataState.Success -> {
                    val state = it.data?.state
                    if (state == "validated") {
                        viewModel.updateScore(false)
                        imagesSelected.clear()
                    } else {
                        imagesSelected.clear()
                    }
                }
            }
        })
    }

    private fun setUpGetChallengeObserver() {
        lifecycleScope.launch {
            viewModel.response.observe(viewLifecycleOwner, {
                when (it) {
                    is DataState.Loading -> {
                        //   Toast.makeText(requireContext(), "Loading ...", Toast.LENGTH_SHORT).show()
                    }
                    is DataState.Error -> {

                        Log.d("response no internet connection : ", "avant")
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                            .show()
                        viewModel.reset()
                    }
                    is DataState.Success -> {
                        setUpChallengeMode(it)
                    }
                }
            })
        }
    }

    private fun setUpChallengeMode(it: DataState<ResponseChallenge>) {
        if (it.data?.dataset?.mode == "classifications") {
            challengeMode = it.data.dataset.mode
            idChallenge = it.data.id
            val myCustomizedString = SpannableStringBuilder()
                .append("Veuillez sélectionner les images de: ")
                .bold {
                    append(it.data.`class`)
                }
            binding.txtQuestion.text = myCustomizedString
            binding.txtQuestion.visibility = View.VISIBLE
            binding.editTextTranscription.visibility = View.INVISIBLE
            binding.textTranscription.visibility = View.INVISIBLE
        } else {
            challengeMode = it.data!!.dataset.mode
            idChallenge = it.data.id

            numberOne = it.data.images[0].id
            numberTwo = it.data.images[1].id

            binding.txtQuestion.visibility = View.INVISIBLE
            binding.editTextTranscription.visibility = View.VISIBLE
            binding.textTranscription.visibility = View.VISIBLE
        }
        setUpAdapter(it)
    }

    private fun setUpAdapter(response: DataState<ResponseChallenge>) {
        val listAdapter =
            ChallengeListAdapter(response.data!!.dataset.id) { image: ImageForAdapter ->
                if (image.selected) {
                    imagesSelected.add(image.id)
                } else {
                    imagesSelected.remove(image.id)
                }
                Log.d("selected", "setUpAdapter: ${imagesSelected.size}")
            }
        binding.myRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 6)
            (layoutManager as GridLayoutManager).spanSizeLookup =
                object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return if (adapter != null) {
                            if (adapter!!.itemCount == 9) {
                                2
                            } else {
                                3
                            }
                        } else -1
                    }
                }
            adapter = listAdapter
        }
        listAdapter.submitList(response.data.images)
    }


}