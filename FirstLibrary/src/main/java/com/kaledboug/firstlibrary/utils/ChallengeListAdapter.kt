package com.kaledboug.firstlibrary.utils

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import coil.load
import com.kaledboug.firstlibrary.R
import com.kaledboug.firstlibrary.model.Image
import com.kaledboug.firstlibrary.model.ImageForAdapter

class ChallengeListAdapter(
    val id: String,
    val itemClickListener: (image: ImageForAdapter) -> Unit
) :
    ListAdapter<Image, itemViewHolder>(Image.DiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): itemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.viewholder_item, parent, false)
        return itemViewHolder(view)
    }

    @SuppressLint("StringFormatInvalid")
    override fun onBindViewHolder(holder: itemViewHolder, position: Int) {
        val item = getItem(position)
        val image = ImageForAdapter(item.id, item.url, false)
        var isSelected = false
        holder.image.load(item.url)

        if (id == "demo_classifications") {
            holder.image.setOnClickListener {
                isSelected = if (isSelected) {
                    holder.linearLayout.setBackgroundResource(R.drawable.notborder)
                    image.selected = false
                    itemClickListener(image)
                    false
                } else {
                    holder.linearLayout.setBackgroundResource(R.drawable.border)
                    image.selected = true
                    itemClickListener(image)
                    true
                }
            }
        }
    }
}