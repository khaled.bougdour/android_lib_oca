package com.kaledboug.firstlibrary.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}