package com.kaledboug.firstlibrary.utils

sealed class DataState<T>(
    val data: T? = null,
    var message: String? = null
) {
    class Success<T>(data: T, status: Status) : DataState<T>(data)
    class Error<T>(message: String, status: Status, data: T? = null) : DataState<T>(data, message)
    class Loading<T>(status: Status) : DataState<T>()
}
