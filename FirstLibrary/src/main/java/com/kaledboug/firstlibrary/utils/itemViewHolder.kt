package com.kaledboug.firstlibrary.utils

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.kaledboug.firstlibrary.R


class itemViewHolder (view : View) : RecyclerView.ViewHolder(view) {
    val image : ImageView = view.findViewById(R.id.myImage)
    val linearLayout : LinearLayout = view.findViewById(R.id.linearLayoutImage)
}