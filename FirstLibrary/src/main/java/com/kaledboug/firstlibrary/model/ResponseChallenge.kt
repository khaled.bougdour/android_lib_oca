package com.kaledboug.firstlibrary.model

import com.kaledboug.firstlibrary.model.Client
import com.kaledboug.firstlibrary.model.Dataset
import com.kaledboug.firstlibrary.model.Image


data class ResponseChallenge(
    val audio_fallback: String,
    val `class`: String?,
    val client: Client,
    val dataset: Dataset,
    val id: String,
    val images: List<Image>
)