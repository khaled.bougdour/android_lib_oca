package com.kaledboug.firstlibrary.model

data class ImageForAdapter(
    val id: String,
    val url: String,
    var selected: Boolean
)
