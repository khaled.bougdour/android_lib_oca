package com.kaledboug.firstlibrary.model

data class ResponseState(
    val state: String
)