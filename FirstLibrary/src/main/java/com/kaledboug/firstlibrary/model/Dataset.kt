package com.kaledboug.firstlibrary.model

import com.squareup.moshi.JsonClass


data class Dataset(
    val id: String,
    val mode: String,
    val version: Int
)