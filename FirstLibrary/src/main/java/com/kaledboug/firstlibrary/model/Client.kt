package com.kaledboug.firstlibrary.model

import com.squareup.moshi.JsonClass

data class Client(
    val color: String,
    val id: String,
    val name: String
)