package com.kaledboug.firstlibrary.model

data class Request(
    val params: Params
)
