package com.kaledboug.firstlibrary.model

data class Params(
    val selection : List<String>,
    val transcriptions : Map<String,String>
)
