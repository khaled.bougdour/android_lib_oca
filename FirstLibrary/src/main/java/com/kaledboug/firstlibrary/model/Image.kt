package com.kaledboug.firstlibrary.model

import androidx.recyclerview.widget.DiffUtil

data class Image(
    val id: String,
    val url: String,
) {
    class DiffCallback: DiffUtil.ItemCallback <Image>() {
        override fun areItemsTheSame(oldItem: Image, newItem: Image): Boolean {
            return oldItem.id == newItem.id
        }
        override fun areContentsTheSame(oldItem: Image, newItem: Image): Boolean {
            return oldItem.id == newItem.id && oldItem.url ==
                    newItem.url
        }
    }
}