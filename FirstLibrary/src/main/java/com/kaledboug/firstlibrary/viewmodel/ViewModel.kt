package com.kaledboug.firstlibrary.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import androidx.lifecycle.ViewModel
import com.kaledboug.firstlibrary.model.Request
import com.kaledboug.firstlibrary.model.ResponseChallenge
import com.kaledboug.firstlibrary.model.ResponseState
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.kaledboug.firstlibrary.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import com.kaledboug.firstlibrary.utils.DataState
import com.kaledboug.firstlibrary.utils.Status
import com.kaledboug.firstlibrary.utils.hasInternetConnection
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import retrofit2.Response
import java.io.IOException

@HiltViewModel
class ViewModel @Inject constructor(
    private val repository: Repository,
) : ViewModel() {

    private val _response: MutableLiveData<DataState<ResponseChallenge>?> = MutableLiveData()
    val response: LiveData<DataState<ResponseChallenge>?> get() = _response

    private val _validate: MutableLiveData<DataState<ResponseState?>?> = MutableLiveData()
    val validate: LiveData<DataState<ResponseState?>?> get() = _validate

    private val _score: MutableLiveData<Int> = MutableLiveData(0)
    val score: LiveData<Int> get() = _score

    private val _step: MutableLiveData<Int> = MutableLiveData(0)
    val step: LiveData<Int> get() = _step


    fun getChallenge(id: String, context: Context) =
        viewModelScope.launch(Dispatchers.IO) {
            _response.postValue(DataState.Loading(Status.LOADING))
            if (hasInternetConnection(context)) {
                repository.getChallenge(id)
                    .catch { exception ->
                        when (exception) {
                            is IOException -> _response.postValue(
                                DataState.Error(
                                    "Network Error : ${exception.message}",
                                    Status.ERROR
                                )
                            )
                            else -> _response.postValue(
                                DataState.Error(
                                    exception.message!!,
                                    Status.ERROR
                                )
                            )
                        }
                    }.collect { dataResponse ->
                        if (dataResponse.code() == 404) {
                            Log.d("getChallengedwasExecuted : ", "404")
                            _response.postValue(
                                DataState.Error(
                                    "Server Not Found",
                                    Status.ERROR
                                )
                            )
                        } else if (dataResponse.code() == 400) {
                            _response.postValue(
                                DataState.Error(
                                    "Bad Request",
                                    Status.ERROR
                                )
                            )
                        }else if (dataResponse.code() == 200){
                            Log.d("response ok: ","avant")
                            _response.postValue(
                                DataState.Success(
                                    dataResponse.body()!!,
                                    Status.SUCCESS
                                )
                            )
                        }
                    }
            } else {

                _response.postValue(
                    DataState.Error(
                        "No Internet Connection",
                        Status.ERROR
                    )
                )
            }
        }

    fun checkChallenge(id_client: String, id: String, request: Request, context: Context) =
        viewModelScope.launch(Dispatchers.IO) {
            _validate.postValue(DataState.Loading(Status.LOADING))
            if (hasInternetConnection(context)) {
                repository.checkChallenge(id_client, id, request)
                    .catch { exception ->
                        when (exception) {
                            is IOException -> _validate.postValue(
                                DataState.Error(
                                    "Network Error : $exception.message!!",
                                    Status.ERROR
                                )
                            )
                            else -> _validate.postValue(
                                DataState.Error(
                                    exception.message!!,
                                    Status.ERROR
                                )
                            )
                        }
                    }.collect { dataResponse ->
                        if (dataResponse?.code() == 404) {
                            _validate.postValue(
                                DataState.Error(
                                    "Server Not Found",
                                    Status.ERROR
                                )
                            )
                        } else if (dataResponse?.code() == 400) {
                            _validate.postValue(
                                DataState.Error(
                                    "Bad Request",
                                    Status.ERROR
                                )
                            )
                        } else if (dataResponse?.code() == 200) {
                            val response: Response<ResponseState> =
                                dataResponse as Response<ResponseState>
                            Log.d("ErrorBodyContent : ", dataResponse.body().toString())
                            Log.d("ErrorBodyContent : ", dataResponse.errorBody().toString())
                            _validate.postValue(
                                DataState.Success(
                                    response.body(),
                                    Status.SUCCESS
                                )
                            )

                            if (_step.value!! < 5) {
                                _step.postValue(_step.value?.plus(1))
                            }

                        }
                            else if (dataResponse?.code() == 403) {
                            val moshi2 = Moshi.Builder()
                                .add(KotlinJsonAdapterFactory())
                                .build()
                            val jsonAdapter: JsonAdapter<ResponseState> = moshi2
                                .adapter(ResponseState::class.java)
                            val response2 = dataResponse.errorBody()!!.string()
                            Log.d("ErrorBodyContent : ", response2)
                            val handledResponse = jsonAdapter.fromJson(response2)
                            _validate.postValue(
                                DataState.Success(
                                    handledResponse,
                                    Status.SUCCESS
                                )
                            )

                            if (_step.value!! < 5) {
                                _step.postValue(_step.value?.plus(1))
                            }
                        }



                    }
            } else {
                _validate.postValue(
                    DataState.Error(
                        "No Internet Connection",
                        Status.ERROR
                    )
                )
            }
        }

    fun updateScore (reset: Boolean){
        if (reset) {
            _score.postValue(0)
        }
        if (!reset) {
            _score.postValue(_score.value?.plus(1))
        }

    }

    fun updateStep(reset: Boolean) = viewModelScope.launch {
        if (reset) {
            _step.postValue(0)
        }
        if (!reset) {
            _step.postValue(_step.value?.plus(1))
        }
    }

    fun reset() {
        _response.value = null
        _validate.value = null
    }
}