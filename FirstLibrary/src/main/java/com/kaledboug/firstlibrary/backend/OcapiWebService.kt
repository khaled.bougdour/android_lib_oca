package com.kaledboug.firstlibrary.backend

import com.kaledboug.firstlibrary.model.Request
import com.kaledboug.firstlibrary.model.ResponseChallenge
import com.kaledboug.firstlibrary.model.ResponseState
import retrofit2.Response
import retrofit2.http.*

interface OcapiWebService {

    @GET("challenge/{client_id}")
    @Headers("Accept:application/json","Content-Type:application/json")
    suspend fun getChallenge(
        @Path("client_id") id: String
    ): Response<ResponseChallenge>


    @POST("check/{client_id}/{challenge_id}")
    @Headers("Accept:application/json","Content-Type:application/json")
    suspend fun checkChallenge(
        @Path("client_id") id_client: String,
        @Path("challenge_id") id: String,
        @Body request: Request
    ): Response<ResponseState>?


}