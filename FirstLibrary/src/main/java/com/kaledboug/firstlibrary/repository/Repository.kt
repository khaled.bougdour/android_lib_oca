package com.kaledboug.firstlibrary.repository


import com.kaledboug.firstlibrary.backend.OcapiWebService
import com.kaledboug.firstlibrary.model.Request
import com.kaledboug.firstlibrary.model.ResponseChallenge
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import retrofit2.Response


import javax.inject.Inject


@ActivityRetainedScoped
class Repository @Inject constructor(
    private val ocapiWebService: OcapiWebService
) {

    suspend fun getChallenge(id: String): Flow<Response<ResponseChallenge>> = flow {
        emit(ocapiWebService.getChallenge(id))
    }.flowOn(Dispatchers.IO)

    suspend fun checkChallenge(
        id_client: String,
        id: String,
        request: Request
    ) = flow {
        val response = ocapiWebService.checkChallenge(id_client, id, request)
        emit(response)
    }.flowOn(Dispatchers.IO)

//    fun checkChallengelass(
//        id_client: String,
//        id: String,
//        request: Request
//    ) {
//        Log.d(
//            "CLASSIFICATION",
//            "checkChallengelass: ${ocapiWebService.checkChallenge(id_client, id, request).code()}"
//        )
//        Log.d(
//            "CLASSIFICATION",
//            "checkChallengelass: ${
//                ocapiWebService.checkChallengeClass(id_client, id, request).enqueue(object : Callback<ResponseCheck> {
//                    override fun onResponse(
//                        call: Call<ResponseCheck>,
//                        response: Response<ResponseCheck>
//                    ) {
//                        Log.d("zbel", "onResponse: ${response.body().toString()} ")
//                    }
//
//                    override fun onFailure(call: Call<ResponseCheck>, t: Throwable) {
//                        TODO("Not yet implemented")
//                    }
//
//                })
//            }"
//        )
// }

}
